use actix_web::{error, web, App, Error, HttpRequest, HttpResponse, HttpServer};
use futures::StreamExt;

const MAX_SIZE: usize = 262_144; // max payload size is 256k

async fn greet(req: HttpRequest, mut payload: web::Payload) -> Result<HttpResponse, Error> {
    // payload is a stream of Bytes objects
    let mut body = web::BytesMut::new();
    while let Some(chunk) = payload.next().await {
        let chunk = chunk?;
        // limit max size of in-memory payload
        if (body.len() + chunk.len()) > MAX_SIZE {
            return Err(error::ErrorBadRequest("overflow"));
        }
        body.extend_from_slice(&chunk);
    }

    let name = format!("Name: {}", body.len());
    let location = format!(
        "http://{}{}/sign",
        req.headers()
            .get("Host")
            .and_then(|v| v.to_str().ok())
            .unwrap_or("localhost"),
        req.uri()
    );

    eprintln!("Signing location header: {}", location);

    Ok(HttpResponse::Ok()
        .content_type("text/plain")
        .header("Location", location)
        .body(name.to_string()))
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .route("/", web::get().to(greet))
            .route("/{name}", web::post().to(greet))
    })
    //.bind(("127.0.0.1", 8080))?
    .bind_uds("/run/user/1000/pks-dummy.sock")?
    .run()
    .await
}
